\section{Decomposability Theory}
\label{sec:theory}

We first formally discuss the condition when a MapReduce program is eligible for partial aggregation
that we characterize using the concept of \emph{decomposability}.
According to our discussion in Section~\ref{sec:back}, decomposability is entirely determined by the reduce function as defined by an initializor, an accumulator and a finalizer.
We simplify our discussion by first ignoring the finalizer and focusing on accumulators that are decomposable based on a necessary and sufficient condition that will also provide a solution about how to construct the combiner.
Section~\ref{sec:drf} will consider including the finalizer in a complete solution.

%\subsection{Formal definition}

If a monolithic reduce function $R$ is eligible for partial aggregation, then the combination of the \hl{InitialReduce}, \hl{Combine} and \hl{FinalReduce} functions should produce the same outputs as $R$.
As discussed in the previous section, a reduce function $R$ is defined by an initial value $s_0$, an accumulator $F$, and an output function $O$.
We denote this relation as $R=\langle s_0; F; O\rangle$.
Until Section~\ref{sec:drf}, we assume that $O$ is the identity function.
%
Given a solution space $S$ and a input space $I$, an accumulator $F$ is a function from $S\times I$ to $S$.
For convenience, we generalize the accumulator $F$ by extending the domain of $F$ to $S\times I^*$ as follows:
\begin{eqnarray}
F(s, \epsilon) &=& s \nonumber\\
F(s, \langle x\rangle\oplus X) &=& F(F(s, x), X)\nonumber
\end{eqnarray}
\noindent Here $I^*$ is the set of input value sequences, $\epsilon$ is the empty sequence, $\oplus$ is the concatenation operator of two sequences, $x\in I$ and $X\in I^*$.
We can then easily prove that $F(F(s, X), Y)=F(s, X\oplus Y)$ for all input sequences $X, Y\in I^*$.
\textbf{Note on notation:} a lower-case letter ($x$, $y$, $\ldots$) denotes one input value, an upper-case letter  ($X$, $Y$, $\ldots$) denotes a sequence of input values, and a sequence of lower-case letters $x_1x_2...x_n$ ($n\geq 1$) denotes an expanded sequence of input values $\langle x_1,x_2,...,x_n\rangle$.

Let $s_0$ be an initial solution, and let $S$ be the solution space defined by $S=\{ s|s=F(s_0, X), X\in I^*\}$. A combiner $C$ is a function from $S\times S$ to $S$.
Inspired by Yu et al.~\cite{Yu:DistributedAggregation}, the commutativity and associativity of $C$ and $F$ can characterize the \emph{decomposability} of $F$, from which we form the following definition.

\begin{definition} [Decomposable accumulator] An accumulator $F$ is \textbf{decomposable} if and only if there is a combiner $C$ such that
\begin{enumerate}
\item The following equation holds for any two input sequences $X_1, X_2\in I^*$:
\begin{equation}\label{eq:2}
F(s_0, X_1\oplus X_2) = C(F(s_0, X_1), F(s_0, X_2))
\end{equation}
\item $F$ is commutative: for any two input sequences $X_1, X_2\in I^*$, $F(s_0, X_1\oplus X_2)=F(s_0, X_2\oplus X_1)$;
\item $C$ is commutative: for any two solutions $s_1, s_2\in S$, $C(s_1, s_2) = C(s_2, s_1)$;
\item $C$ is associative: for any three solutions $s_1, s_2, s_3\in S$, $C(C(s_1, s_2), s_3)=C(s_1, C(s_2, s_3))$.
\end{enumerate}
We say that $C$ is the \textbf{decomposed combiner} of $F$.\label{def:decomposable}
\end{definition}

%\noindent $X_i$ is the output from the $i$-th mapper, and so $X$ is the collection of outputs from all mappers. Because the shuffling phase does not guarantee the order in which the outputs are concatenated, we allow $X_1\oplus...\oplus X_n=\pi(X)$ for any permutation function $\pi$. The left hand side of Equation~\ref{eq:1}, $F(s_0, X)$, is the output of the original reduce function; each constituent of the right hand side of Equation~\ref{eq:1}, $F(s_0, X_i)$, is the output of the \hl{InitialReduce} function after $i$-th mapper, where the right hand side as a whole is then the output of the \hl{Combine} function.
%
\noindent Our problem is then to decide the decomposability of an accumulator and find a decomposed combiner:

\begin{pdef}
Given a reducer $R$ whose accumulator is $F$, check the decomposability of $F$ and, if $F$ is decomposable, then find a decomposed combiner $C$.
\end{pdef}

\subsection{A Necessary and Sufficient Condition}

\noindent By carefully examining the four conditions in definition~\ref{def:decomposable}, we find that conditions 3 and 4 can be derived from conditions 1 and 2, and so we can form the following less redundant lemma:

\begin{lemma} Given that an accumulator $F$ is decomposable, a combiner $C$ such that Equation~\ref{eq:2} holds true, and that $F$ is commutative, then $C$ is commutative and associative.
\label{lemma:less-redundant}
\end{lemma}
\begin{proof} For the commutativity, given any two solutions $s_1, s_2\in S$, according to the definition of $S$, we know there are two input sequences $X, Y\in I^*$ such that $s_1=F(s_0, X), s_2=F(s_0, Y)$. Therefore $C(s_1, s_2)=C(F(s_0, X), F(s_0, Y))=F(s_0, X\oplus Y)=F(s_0, Y\oplus X)=C(s_2, s_1)$. The associativity of $C$ can be proved similarly by noting that $(X\oplus Y)\oplus Z=X\oplus (Y\oplus Z)$ holds for any three input sequences $X, Y, Z\in I^*$.
\end{proof}

\noindent We next prove that if the accumulator $F$ is commutative, then the combiner $C$ can always be constructed according to Equation~\ref{eq:2}.
However, we first note that the commutativity of $F$ has an alternative form that does not depend on the set of input sequences ($I^*$), which we use to form the following lemma:

\begin{lemma}
$F$ is commutative if and only if for any solutions $s\in S$, and any two input values $x,y\in I$, $F(s, xy)=F(s, yx)$.
\label{lemma:com-acc}
\end{lemma}

\begin{proof} The proof is in our uploaded TR.
\end{proof}

\noindent We are interested in this alternative form because proving the commutativity of an accumulator only requires enumerating an input space of finite dimension $S\times I\times I$, instead of an input space of infinite dimension $I^*\times I^*$. This alternative form is then more amenable for the programming verification tools and SMT solvers that are used in Section~\ref{sec:alg}.

To show that the commutativity of $F$ essentially determines the decomposability of $F$, we would like to first show how to determine the decomposed combiner if $F$ is decomposable.
Given any two solutions $s_1, s_2\in S$, according to the definition of $S$, we know there are two input sequences such that $F(s_0, X)=s_1, F(s_0, Y)=s_2$. So according to Equation~\ref{eq:2}, we have:
\begin{equation}
C(s_1, s_2) = C(F(s_0, X), F(s_0, Y)) = F(s_1, Y)\label{eq:3}
\end{equation}

\noindent The decomposed combiner $C$ can then be calculated using $F$ according to Equation~\ref{eq:3}, if we can calculate $Y$ according to $s_2$. However, there might be many possible such $Y$-s, satisfying $F(s_0, Y)=s_2$. %, i.e. if $Y$ satisfies $F(s_0, Y)=s_2$, since $F$ is commutative, any permutation of $Y$ should also satisfy this equation.
In the following, we shall show that if $F$ is commutative, then for all $Y$ satisfying $F(s_0, Y)=s_2$, $F(s_1, Y)$ are equal to each other, and therefore $C$ can be uniquely determined by $F$.
% no paragraph break.
To achieve this goal, we define the concepts of a \emph{general inverse function} and a \emph{derived combiner} that decompose $F$, and show that if an accumulator $F$ is commutative, then for all general inverse functions, the derived combiners are exactly the same. Combining these conclusions, we will then prove that the commutativity of $F$ determines its decomposability, and any derived combiner is in fact the composed combiner. We define the general inverse function and derived combiner as follows:
\begin{definition} [General inverse function] A function $H : S\rightarrow I^*$ is a \textbf{general inverse function} of an accumulator $F:S\times I^*\rightarrow S$, if and only if for all $s\in S$, $F(s_0, H(s)) = s$.
\label{def:gif}
\end{definition}

\begin{definition} [Derived combiner] Given an accumulator $F$ and a general inverse function $H$ of $F$, a \textbf{derived combiner} $C_H$ related to $F$ and $H$ is defined as $C_H(s_1, s_2) = F(s_1, H(s_2))$.
\label{def:dcom}
\end{definition}

\noindent Intuitively, the general inverse function $H$ of an accumulator $F$ defines an input sequence $Y$ for each solution $s\in S$ such that $F(s_0, Y)=s$.
Becase derived and decomposed combiners (Equation~\ref{eq:3}) have a similar form, if the derived combiner $C_H$ is the same for any general inverse function $H$ of $F$, and if $F$ is commutative, then the derived combiner is exactly the decomposed combiner.
The following three lemmas aid in proving that this is true:
% To prove that this is true, we first prove the existence of the general inverse functions:

\begin{lemma} There exists a general inverse function for any accumulator $F$.
\end{lemma}
\begin{proof} For any solution $s\in S$, by the definition of $S$, we know there is an input sequence $X_s$ such that $F(s_0, X_s) = s$. We define a function $H:S\rightarrow I^*$ to be $H(s) = X_s$, then we know $H$ is a general inverse function of $F$.
\end{proof}

%\noindent We must next prove the following lemma (XXX why???):

\begin{lemma} If an accumulator $F$ is commutative, then for any two input sequences $X, Y\in I^*$ such that $F(s_0, X)=F(s_0, Y)$, $F(s, X)=F(s, Y)$ holds true for any $s\in S$, .
\label{lemma:any-input}
\end{lemma}

\begin{proof} For any $s\in S$, we know there is an input sequence $Z$ such that $F(s_0, Z) = s$. Therefore, we have
\begin{eqnarray}
F(s, X) &=& F(F(s_0, Z), X) = F(s_0, Z\oplus X) \nonumber\\
&=& F(s_0, X\oplus Z)=F(F(s_0, X)\oplus Z)\nonumber\\
&=& F(F(s_0, Y)\oplus Z) = F(s_0, Y\oplus Z)\nonumber\\
&=& F(s_0, Z\oplus Y)=F(F(s_0, Z), Y) = F(s, Y)\nonumber
\end{eqnarray}
\end{proof}

%\noindent This lemma then allow us to prove the following lemma:

\begin{lemma} If an accumulator $F$ is commutative, then for any two general inverse functions $H$ and $H'$ of $F$, $C_H\equiv C_{H'}$.
\label{lemma:dc-equiv}
\end{lemma}

\begin{proof}
For two solution $s_1, s_2\in S$, since $F(s_0, H(s_2))=s_2=F(s_0, H'(s_2))$, according to Lemma~\ref{lemma:any-input}, we have that $C_H(s_1, s_2) = F(s_1, H(s_2)) = F(s_1, H'(s_2)) = C_{H'}(s_1, s_2)$.
\end{proof}

\noindent Combining all above conclusions allows us to prove the following theorem:

\begin{theorem} An accumulator $F$ is decomposable if and only if $F$ is commutative. The decomposed combiner $C$ is uniquely determined by $F$, and takes the form $C(s_1, s_2)=F(s_1, H(s_2))$ where $H$ is any general inverse function of $F$.
\end{theorem}

\begin{proof} $\Leftarrow$ is obvious. For $\Rightarrow$, according to Lemma~\ref{lemma:dc-equiv}, we know that the derived combiner $C$ is uniquely determined by $F$. We only need to show that $C$ is the decomposed combiner of $F$, by showing that $F$ and $C$ satisfy Equation~\ref{eq:2}. In fact, for any two input sequences $X, Y\in I^*$, we can choose a general inverse function $H$ of $F$ such that $H(F(s_0, Y)) = Y$. Then we have $C(F(s_0, X), F(s_0, Y)) = C_H(F(s_0, X), F(s_0, Y)) = F(F(s_0, X), H(F(s_0, Y)))
= F(F(s_0, X), Y) = F(s_0, X\oplus Y)$.
\end{proof}

\subsection{Decomposable Reduce Function}
\label{sec:drf}

We extend our discussion to consider the output function $O$ by first defining the concept of a \emph{decomposable reduce function}:

\begin{definition} [Decomposable reducer] A reduce function $R=\langle s_0; F; O\rangle$ is \textbf{decomposable} if and only if there is a combiner $C$ such that
\begin{enumerate}
\item The following equation holds for any two input sequences $X_1, X_2\in I^*$:
\begin{equation}
O(F(s_0, X_1\oplus X_2)) = O(C(F(s_0, X_1), F(s_0, X_2)))
\end{equation}
\item $F$ is commutative: for any two input sequences $X_1, X_2\in I^*$, $O(F(s_0, X_1\oplus X_2))=O(F(s_0, X_2\oplus X_1))$;
\item $C$ is commutative: for any two solutions $s_1, s_2\in S$, $O(C(s_1, s_2))=O(C(s_2, s_1))$;
\item $C$ is associative: for any three solutions $s_1, s_2, s_3\in S$, $O(C(C(s_1, s_2), s_3))=O(C(s_1, C(s_2, s_3)))$.
\end{enumerate}
We call $C$ the \textbf{decomposed combiner} of the reduce function $R$
\end{definition}

\begin{pdef}
Check the decomposability for a given reduce function $R$. If $R$ is decomposable, find a decomposed combiner $C$.
\end{pdef}

\noindent Based on reasoning very similar to that in the previous subsection, we can prove the following theorem:

\begin{theorem} A reduce function $R$ defined by an initial value $s_0$, an accumulator $F$, and an output function $O$, is decomposable if and only if for any solution $s\in S$, and any two input values $x, y\in I$, $O(F(s, xy)) = O(F(s, yx))$. The decomposed combiner $C$ can be any derived combiner $C_H(s_1, s_2)=F(s_1, H(s_2))$ where $H$ is any general inverse function of $F$.
\label{thm:dr}
\end{theorem}

\noindent The decomposed combiner is no longer uniquely determined nor is it necessarily the same as the dervied combiner. However, we can still construct a decomposed combiner by calculating a general inverse function of the accumulator $F$.
