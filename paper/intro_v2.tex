\section{Introduction}\label{sec:intro}

MapReduce~\cite{Dean:2004} is a programming model for the scalable processing of large data sets on machine clusters where grouping and aggregation are encoded as \emph{map} and key-grouped \emph{reduce} functions.
Records in MapReduce are mapped to key/value pairs on multiple machines where values are transmitted, for each key, to single machines that compute final results as an ``all-at-once'' reduction; this transmission can be a bottleneck that involves substantial network I/O and latency.
This paper focuses on a common partial aggregation optimization where values with the same keys are first aggregated on the mapping machines into partial results, which are then transmitted to reducing machines to be \emph{combined} into final results; network I/O is reduced substantially since these partial results are much smaller than the records that would have been transmitted instead.
Unfortunately, the programming overhead to take advantage of partial aggregation is substantial: our own empirical study of real MapReduce-style {\scope}~\cite{Chaiken:scope} programs reveals that many programs that could take advantage of partial aggregation do not, while other programs take advantage of partial aggregation in dubious ways that can cause non-determinism.

Partial aggregation is currently enabled by having the programmer manually divide reduce functionality into three functions: initial-reduce, combine, and final-reduce.
Given the trickiness of encoding partial aggregation manually, this paper proposes that a tool first automatically \emph{verify} if a MapReduce program is eligible for partial aggregation and, if it is, \emph{synthesize} the combine and initial/final reduce functions automatically.
Although Yu et al.~\cite{Yu:DistributedAggregation} defined the concept of decomposability of a reduce function to characterize programs that are eligible for partial aggregation, it is still not known how to directly check for such decomposability.
Additionally, even though the definition of decomposability reveals declarative constraints for what the combine and initial-final reduce functions must look like, the constraints do not by themselves reveal how to synthesize these functions.

We examined real {\scope} programs to identify common properties that could be used to simplify the problem of decomposability.
First, all reduce functions involve a loop that enumerates all records in a group using an \emph{accumulator}.
Second, the initial/final-reduce function of most decomposable reduce functions can be simply constructed by rearranging the code of the reduce function, and so we only need to synthesize the combine function.
Finally, the decomposability of a reduce function is determined by the algebraic properties of the combine function and accumulator.

Based on these properties, we solve the problem of verification and synthesis for partial aggegation in three steps.
The theoretical foundation of our solution is provided by proving a necessary and sufficient condition to show that the decomposability of a reduce function is entirely determined by the commutativity of the accumulator, while the combine function can be constructed by finding an inverse function of the accumulator.
We next show how to use this condition to convert the decomposability problem into a standard program verification problem that can be solved using a SMT solver.
Finally, we attempted to convert our combine function synthesis problem into a standard program synthesis problem by showing that it is a program inversion problem.
Unfortunately, we failed because we face a nondeterministic program inversion problem that is not addressed by state-of-the-art work~\cite{pins}.
However, we observe that most of our target functions have inverse functions of a form that conform to at least one of three non-trivial special cases for which we can design special-case combiner synthesis algorithms.

We implement a prototype, and evaluate it over real jobs. The results show that our prototype is able to find eligible reducers that do not leverage the partial aggregation originally. Our prototype succeed in generating the combiner for 90.9 \% of these reducers, and our performance experiments show that by enabling partial aggregation, there is an up to 55\% latency improvement, and up to 99.98 \% shuffling IO reduction.

The rest of the paper is organized as follows. Section~\ref{sec:back} provides preliminary background on both MapReduce and partial aggregation. Section~\ref{sec:theory} formally studies the problem of automated partial aggregation while we develop in Section~\ref{sec:alg} algorithms based on our formal conclusions to solve the verification and synthesis problems. The practical issues and limitations of our solution are discussed in Section~\ref{sec:prac}. Section~\ref{sec:study} reports on our empirical results on a study of real SCOPE programs as well as the feasibility of our algorithms. Section~\ref{sec:rel} presents related work and Section~\ref{sec:conc} concludes.
