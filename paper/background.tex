\section{MapReduce and Partial Aggregation}
\label{sec:back}

\newcommand{\hl}[1]{\text{\texttt{\small{#1}}}}

% \subsection{MapReduce framework}% - really bad style to begin section with sub section.

MapReduce~\cite{Dean:2004} is a programming model that handles large-scale data processing problems through distributed computing on clusters of computers.
A MapReduce program is defined by a \texttt{Map} function that maps records into key/value pairs, and a \texttt{Reduce} function that aggregates the values of each key to produce a result.
Figure~\ref{fig:ex-mr} shows as an example of the code for a typical MapReduce implementation that calculates the average clicks on each URL.

\begin{figure}
{\small{\begin{verbatim}
Map(Iterable<Record> records) {
  foreach (record in records) {
    Text url = record.url;
    Int clicks = record.clicks;
    emit(url, clicks);
  }
}
Reduce(Text key, Iterable<Int> values) {
  int sum = 0;
  int count = 0;
  foreach (i in values) {
    sum += i;
    count ++;
  }
  emit(key, sum / count);
}
\end{verbatim}}}
\caption{An example MapReduce program to calculate the average clicks.}
\label{fig:ex-mr}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.35\textwidth]{resource/MapReduceOriginal.pdf}
\caption{MapReduce execution.}
\label{fig:mr-frame}
\end{figure}

The typical execution of a MapReduce program on a computer cluster is illustrated by Figure~\ref{fig:mr-frame}. 
The MapReduce scheduler first launches ``mappers'' on idle machines that process input data to generate key/value pairs via an outputting \hl{emit} method, which are then sorted by key (\emph{local sort}).
% no paragraph break
A \emph{partitioner} during a \emph{shuffling phase} then assigns each key to a ``reducer'' to process all values with this key. 
Reducers are scheduled on idle machines where all mappers transmit values to reducers according to assigned keys; the reducers then perform a \emph{merge sort} to group all received values together by key.
% no paragraph break
Finally, each reducer executes the reduce function over these value pairs to generate outputs for its key, where most reduce functions contain what is commonly called a \emph{main foreach loop} that enumerates all (and only) values that share the same key.

%\subsection{Partial Aggregation}

\begin{figure}
\centering
\includegraphics[width=0.35\textwidth]{resource/MapReduceLocalReduce.pdf}
\caption{MapReduce execution with the partial aggregation optimization.}
\label{fig:mr-lr-frame}
\end{figure}

The shuffling phase incurs lots of network I/O as values are transmitted from mapper to reducer machines, which is the bottleneck of many MapReduce programs.
Most MapReduce-style systems then support the use of \emph{partial aggregation} to enable the partial reduction of values into partial results on mapper machines, where the partial results are transmitted to reducers that combine them into final results.
Network I/O is reduced substantially because only partial results, which are usually smaller than lists of values, are transmitted from mappers to reducers.
Partial aggregation is enabled by encoding reduce functionality as three functions, \hl{InitialReduce}, \hl{Combine}, and \hl{FinalReduce}; the execution of the MapReduce program is then altered as illustrated in Figure~\ref{fig:mr-lr-frame}.
After each mapper performs the local sort phase, the \hl{InitialReduce} function is executed for each set of values of the same key to compute their partial results.
Partial results for each key are transmitted to reducers during the shuffling phase, where they are combined via the \hl{Combine} function and finally transformed into the final result using the {\tt FinalReduce} function.
%
Figure~\ref{fig:ex-pa} provides functions that enable partial aggregation for the average clicks example in Figure~\ref{fig:ex-mr}.
Note that the \hl{Initial\-Reduce} and \hl{Combine} functions are very similar, differing only in the kinds of data they operate on, which should not be surprising given that reduce computation is commutative.

\begin{figure}
{\small{\begin{verbatim}
InitialReduce(Text key, Iterable<Int> values) {
  int sum = 0;
  int count = 0;
  foreach (i in values) {
    sum += i;
    count ++;
  }
  emit(key, new SumCountPair(sum, count));
}
Combine(Text key, Iterable<SumCountPair> values) {
  int sum = 0;
  int count = 0;
  foreach (i in values) {
    sum += i.key;
    count += i.count;
  }
  emit(key, new SumCountPair(sum, count));
}
FinalReduce(Text key, SumCountPair value) {
  emit(key, value.sum / value.count);
}
\end{verbatim}}}
\caption{\hl{InitialReduce}, \hl{Combine}, and \hl{FinalReduce} functions that enable partial aggregation for the example in Figure~\ref{fig:ex-mr}.}
\label{fig:ex-pa}
\end{figure}


%If a MapReduce program is eligible for partial aggregation optimization, we say that the reduce function is \emph{decomposable}.

% \subsection{Observations}

Intuitively, a MapReduce program is  ``eligible'' for partial aggregation only if its computation is commutative, meaning that the order that values are processed in the reducer function is irrelevant to the final result being computed.
We observe via a study that most MapReduce programs that are eligible for the partial aggregation optimization have similar characteristics.
First, most of their \hl{Reduce} functions have the following form:
{\small{\begin{verbatim}
   Reduce(Key, Iterable<Value> values) {
1:   s = s0;
2:   foreach (x in values) {
3:     s = F(s, x);
4:   }
5:   emit(O(s));
   }
\end{verbatim}}}

\noindent Here \hl{s} is a set of \emph{solution variables} that store a partial solution and are initialized to a set of constant values represented by \hl{s0} in the \emph{initializer} assignment on line 1.
Next, lines 3--5 form the main foreach loop that calculates the aggregation by enumerating over the input values, updating values of \hl{s} according to a function \hl{F(s, x)} that we call the \emph{accumulator}.
Finally, line 5 calculates the output value based on the solution variable using a function \hl{O} that we refer to as the \emph{finalizer}.
The reduce is then defined entirely by initial values \hl{s0}, an accumulator \hl{F}, and a finalizer \hl{O}.

For most of the MapReduce programs eligible for the partial aggregation optimization, the \hl{InitialReduce} function is composed by the initializer and the accumulator as follows, while the \hl{FinalReduce} function is exactly the finalizer:
{\small{\begin{verbatim}
InitialReduce(Key key, Iterable<Value> values) {
  s = s0;
  foreach (x in values) {
    s = F(s, x);
  }
  emit(key, s);
}
FinalReduce(Text key, PartialSolution s) {
  emit(key, O(s));
}
\end{verbatim}}}

\noindent The \hl{Combine} function is then defined by a new \emph{combiner} function \hl{C(s, x)} that combines two partial solutions as follows:
{\small{
\begin{verbatim}
Combine(Key key, Iterable<PartialSolution> values) {
  s = s0;
  foreach (x in values) {
    s = C(s, x);
  }
  emit(s);
}
\end{verbatim}
}}

\noindent For the example in Figure~\ref{fig:ex-pa}, the solution variable is the tuple formed from \hl{sum} and \hl{count} while the accumulator \hl{F} is defined as $\hl{F}(\langle{\hl{sum}}, \hl{count}\rangle, \hl{clicks}) = \langle{\hl{sum}}+\hl{clicks}, \hl{count}+1\rangle$. The output function \hl{O} is defined as $\hl{O}(\langle{\hl{sum}}, \hl{count}\rangle)=\hl{sum}/\hl{count}$ while the combiner \hl{C} is defined as $\hl{C}(\langle${\hl{sum1}}, {\hl{count1}}$\rangle,\langle${\hl{sum2}}, {\hl{count2}}$\rangle)=\langle${\hl{sum1}}$+$\hl{sum2}, \hl{count1}$+$\hl{count2}$\rangle$.

%If a reduce function can be decomposed into three supplementary functions, the outputs should remain the same. To simplify the problem, we consider restrict that after executing line 1-4 of the reduce function, the solution variables should have the same value of the ones after executing the {\tt InitialReduce} and the {\tt Combine} function. Therefore, the reduce function is entirely
