\section{Algorithms for Decomposability}
\label{sec:alg}

The last section proved Theorem~\ref{thm:dr} that provides a criterion to verify the decomposability of a reduce function $R=\langle s_0; F; O\rangle$ as well as a method on how to synthesize the {\tt Combine} function.
This section describes how to automate verification and synthesis algorithmically.
To do this, we must first ``understand'' the computation performed by an accumulator by converting a program into a \emph{path formula}. % using symbolic execution.
Then we show that the decomposability checking problem is essentially a satisfiability checking problem that can be fed into a SMT solvers.
We further show that the combiner synthesis problem is primarily a nondeterministic program inversion problem that cannot be handled using existing synthesis techniques.
We develop a new technique to tackle this challenge by observing that most accumulators can be classified into three categories according to their computation,
we find a criterion for each category to check if an accumulator belongs to it, and design an algorithm to synthesize the combiner for it.
Both the criterion verification problem and the synthesis problem depend on SMT solvers to check satisfiability and find solutions.
Despite the satisfiability problem's high complexity, industry-strength SMT solvers can leverage heuristic techniques to answer many SMT queries efficiently.
On the other hand, SMT solvers such as Z3~\cite{z3} cannot yet handle quantifiers very well, and so we ensure that our queries are free of quantifiers before they are submitted to SMT solvers.

We focus our discussion on the following simple language:
{\small{\begin{eqnarray}
f &::=& \lambda [x,...,x]. s\nonumber\\
s &::=& x := e~|~x,...,x := f(e,...,e) ~|~s;s~ |\nonumber\\
&&~ \textbf{if}~ (p)~ s ~\textbf{else}~ s~|~\textbf{return}~e,...,e~|~\textbf{skip}\nonumber\\
e &::=& x~~ |~~ e~ op_a~ e~~|~~ C \nonumber\\
p &::=& e~ op_r~ e~|%~p\wedge p~|~p\vee p~|~\neg p|
~\textbf{true}~|~\textbf{false}\nonumber
\end{eqnarray}}}
\\
This language only considers integer and real types and does not allow for loops and recursive function calls so we can guarantee that every function can be converted into a \emph{path formula}. For simplicity, we also do not allow input variables to be reassigned in a function.
Section~\ref{sec:prac} considers other data types such as arrays and other kind of statements such as loops.

\subsection{Path formula}
\label{sec:formulae}

To fully exploit a function's semantics, we first convert a function into a \emph{path formula} that is defined as follows:

\begin{definition}[Output formula] An \textbf{output formula} of a function $f=\lambda \overline{x}. s$ is a propositional formula in the following form:
\begin{equation}
(\bigvee_{i\in I}\Phi_i(\overline{x}))\wedge\bigwedge_{i\in I} \Phi_i(\overline{x})\rightarrow \overline{o}=\overline{\phi_i}(\overline{x})
\label{eq:good-form}
\end{equation}
We require that for every interpretation of $\langle\overline{x}, \overline{o}\rangle$ satisfying this formula, the equation $f(\overline{x}) = \overline{o}$ is true.
\end{definition}
\begin{definition}[Path formula] A \emph{path formula} of a function $f=\lambda \overline{x}. s$ is an output formula in the form of~\ref{eq:good-form}, such that (a) for any $\overline{x}$, $\bigvee_{i\in I}\Phi_i(\overline{x})\equiv\top$; (b) if $i,j\in I$ and $i\neq j$, then for any $\overline{x}$, $\Phi_i\wedge\Phi_j\equiv\bot$; and (c) for any $i\in I$, $\Phi_i$ is satisfiable.
\end{definition}
%Here we say an interpretation $I$ of a set $V$ of variables to denote a mapping which maps each variable in $V$ to a constant. Furthermore, we say an interpretation $I$ satisfies a formula if an only if after substituting each variable $v\in V$ in the formula with $I(v)$, the formula becomes a tautology.

\noindent \textbf{On notation:} $I$ is a finite index set; $\Phi_i$ is a conjunction of predicates containing only variables in $\overline{x}$; $\overline{o}$ represent the output of $f$; and $\overline{\phi_i}$ is a set of expressions $e$ containing only variables in $\overline{x}$. We use $\overline{x}$, $\overline{o}$ and $\overline{\phi}$ to indicate a vector of variables or expressions. The equality between two vectors $\overline{a}=\overline{b}$ is a conjunction $\bigwedge a_i=b_i$ of equalities between the corresponding items in each vector, while the inequality between two vectors $\overline{a}\neq\overline{b}$ is a disjunction $\bigvee a_i\neq b_i$ of inequalities between the corresponding items in each vector.

A path formula of a function $f$ intuitively expresses its set of execution paths.
Each statement $\Phi_i(\overline{x})\rightarrow \overline{o}=\phi_i(\overline{x})$ is composed by a path condition $\Phi_i(\overline{x})$  and the output value $\phi_i(\overline{x})$.
The output formula enforces that a function will generate an output $\phi_i(\overline{x})$ when it runs over a certain path $\Phi_i(\overline{x})$.
The (a) requirement of the path formula guarantees that the formula contains all possible execution pathes of $f$ while the (b) requirement  guarantees that no two path conditions will be satisfied at the same time. %As an example, consider the path formula for example function $F$ in Figure~\ref{ex:pathf}.
According to the (a) requirement, a path formula has the following form:
\[\bigwedge_{i\in I} \Phi_i(\overline{x})\rightarrow \overline{o}=\overline{\phi_i}(\overline{x})\]

\begin{exmp}
Let us consider the following code.
{\small{\begin{verbatim}
  F = lam[l, s, x].
    if (l==1) then return 0, 10 + x;
    else           return l, s + x;
\end{verbatim}}}
A path formula of $F$ is
\begin{equation}
\begin{aligned}
l=1     \rightarrow o_1=0 ~&\wedge~  o_2=10 +~x\nonumber ~\wedge
\\
l\neq 1 \rightarrow o_1=l ~&\wedge~  o_2=s~~ +~x\nonumber
\end{aligned}
\end{equation}
%\caption{An example function $F$ and its path formula.}
\label{ex:pathf}
\end{exmp}

\noindent Our definition of a path formula is equivalent to the program summary proposed in~\cite{SMART} that takes the form of $\bigvee \Phi_i\wedge \overline{o}=\overline{\phi}$ according to the following lemma:

\begin{lemma} Given function $f=\lambda \overline{x}.s$, path formula $\bigwedge_{i\in I}\Phi_i(\overline{x})\rightarrow \overline{o}=\phi(\overline{x})$ of $f$, and a set of formulae $\alpha_i (i\in I)$. The following equation holds:
\[\bigwedge_{i\in I} (\Phi_i(\overline{x}) \rightarrow \alpha_i) \equiv \bigvee_{i\in I} (\Phi_i(\overline{x})\wedge\alpha_i)\]\label{prop:nb}
\end{lemma}
\begin{proof}
The proof is in our uploaded TR.
\end{proof}
\noindent We prefer output and path formula because they are easy to convert back into programs, easing combiner synthesis.
%In fact, given any formula in the form of $(\bigvee_{i\in I}\Phi_i(\overline{x}))\wedge\bigwedge_{i\in I}\Phi_i(\overline{x})\rightarrow \overline{o}=\phi(\overline{x})$, such that for any two different $\Phi_i$ and $\Phi_j$, $\Phi_i(\overline{x})\wedge\Phi_j(\overline{x})\equiv\bot$ holds for all $\overline{x}$, it is an output formula of the following function:
%{\small{\begin{verbatim}
%    F=lam[x].
%    if (cond_1) then
%        return output_1;
%    else if (cond_2) then
%        return output_2;
%    ...
%\end{verbatim}}}
%\noindent Here {\tt cond\_}$i$ is $\Phi_i(\overline{x})$ and {\tt output\_}$i$ is $\phi_i(\overline{x})$. Accordingly, such a form is beneficial when solving the combiner synthesis problem.
%
We use symbolic execution techniques to generate path formulas where our symbolic execution rules are provided in Figure~\ref{fig:se-rule}.
We maintain a set $E$ and a formula $A$ during execution where, intuitively, $E$ contains all unreturned paths and $A$ contains the partial path formula containing all returned path.
$E$ formally contains a set of path-environment pairs $(\Phi, \sigma)$, where $\Phi$ is a path condition and $\sigma$ is a mapping from variables to their bound expressions.
A path-environment pair $(\Phi, \sigma)$ indicates that when the program executes path $\Phi$, the environment should be $\sigma$.
%We can easily extend the domain of $\sigma$ to the set of all expressions by additionally defining $\sigma(e_1~op_a~e_2)=\sigma(e_1)~op_a~\sigma(e_2)$ and $\sigma(C) = C$, and to the set of all predicates by defining $\sigma(e_1~op_r~e_2)=\sigma(e_1)~op_r~\sigma(e_2)$, $\sigma(\textbf{true})=\textbf{true}$, and $\sigma(\textbf{false})=\textbf{false}$.
%The substitution $[e/x]\sigma$ is a mapping $\sigma'$ such that $\sigma'(x)=e$ and $\sigma'(y)=\sigma(y)$ for all $y\neq x$.
The FUNC-rule's $\sigma_{\overline{x}}$ maps each variable in $\overline{x}$ to itself; while in the IF-rule, we use the notation $p\wedge E$ to denote the set $\{(\sigma(p)\wedge\phi, \sigma): (\phi, \sigma)\in E, \sigma(p)\wedge\phi\nRightarrow \bot\}$.

\begin{figure}[t]
{\small{
ASSIGN \\
\\
\AxiomC{$E'=\{(\Phi, [\sigma(e)/x]\sigma) : (\Phi, \sigma)\in E\}$}
\UnaryInfC{$\langle x:=e; E; A\rangle \mapsto \langle E'; A\rangle$}
\DisplayProof\\
\\
\\
FUNC\\
\\
\AxiomC{$f=\lambda \overline{y}.S$}
\AxiomC{$\langle S;\{(\top, \sigma_{\overline{y}})\};\top\rangle\mapsto\langle \emptyset;\bigwedge_{i\in I} \Phi_i\rightarrow \textbf{o}=\phi_i\rangle$}
\noLine
\BinaryInfC{$E'=\{(\Phi\wedge\Phi', [\phi'/x]\sigma) : $}
\noLine
\UnaryInfC{~~$(\Phi, \sigma)\in E, \Phi'=[\sigma(e)/y]\Phi_i, \phi'=[\sigma(e)/y]\phi_i, \Phi\wedge\Phi'\nRightarrow \bot\}$~~}
\UnaryInfC{$\langle (x,...,x := f(\overline{e})); E; A\rangle\mapsto \langle E'; A\rangle$}
\DisplayProof\\
\\
\\
SEQ \\
\\
\AxiomC{$\langle S_1; E; A\rangle\mapsto\langle E_1; A_1\rangle$}
\AxiomC{$\langle S_2; E_1; A_1\rangle\mapsto\langle E_2; A_2\rangle$}
\BinaryInfC{$\langle (S_1; S_2); E; A\rangle\mapsto\langle E_2; A_2\rangle$}
\DisplayProof\\
\\
\\
IF \\
\\
\AxiomC{$\langle S_1; p\wedge E; \top \rangle\mapsto\langle E_1; A_1\rangle$}
\AxiomC{$\langle S_2; \neg p\wedge E; \top \rangle\mapsto\langle E_2; A_2\rangle$}
\BinaryInfC{$\langle (\textbf{if}~ (p)~ S1 ~\textbf{else}~ S2); E; A\rangle\mapsto\langle E_1\cup E_2; A\wedge A_1\wedge A_2\rangle$}
\DisplayProof\\
\\
\\
RETURN \\
\\
\AxiomC{$A_1=\bigwedge_{(\Phi, \sigma)\in E} \Phi\rightarrow \textbf{o} = \sigma(e_1,...,e_n)$}
\UnaryInfC{$\langle (\textbf{return}~ e_1,...,e_2); E; A\rangle\mapsto \langle \emptyset; A\wedge A_1\rangle$}
\DisplayProof\\
\\
\\
SKIP \\
\\
\AxiomC{}
\UnaryInfC{$\langle \textbf{skip}; E; A\rangle\mapsto\langle E; A\rangle$}
\DisplayProof\\
}}
\caption{Symbolic execution rules for path formula generation.}
\label{fig:se-rule}
\end{figure}

To convert a function $f=\lambda\overline{x}.S$, we run our symbolic execution algorithm to get $\langle S;\{(T, \sigma_{\overline{x}})\};\top\rangle\mapsto\langle E; A\rangle$ where $A$ is a path formula of $f$. If $E$ is not empty, then $(\neg\bigvee_{(\Phi, \sigma)\in E}\Phi)\wedge A$ is equivalent to an output formula of $f$. Otherwise, $A$ is a path formula of $f$. We only consider functions where all paths return a value. Our algorithm slightly differs from traditional symbolic execution in that it targets generating a path formula while traditional algorithms focus on testing a program.
%; and (2) we take function calls into consideration.
The correctness of our algorithm is guaranteed by the following theorem:

\begin{theorem} Given $f=\lambda \overline{x}.S$, if $\langle S;\{(\top, \sigma_{\overline{x}})\};\top\rangle\mapsto\langle \emptyset; A\rangle$, then $A$ is a path formula of $f$.\label{thm:correct}
\end{theorem}
\begin{proof} The proof is in our uploaded TR.
\end{proof}

\subsection{Verifying Decomposability Using an SMT Solver}
\label{sec:dec-check}

We are now ready to show that the decomposability checking problem is a standard program verification problem. To verify if a reducer $R=\langle s_0;F;O\rangle$ is decomposable, we check if $O(F(s, xy))=O(F(s, yx))$ holds for any $s\in S$, $x,y\in I$. We will show that this condition verification problem is a satisfiability checking problem so that we can leverage SMT solvers to handle it. %which is also important for the combiner synthesis problem.
%Such a conversion will provide some insights to other condition verification problems which are important for combiner synthesis, but cannot be converted into a program verification problem.
This condition is in fact $\forall s,x,y. O(F(s, xy))=O(F(s, yx))$ whose the left-hand side is equivalent to the following function:
{\small{\begin{verbatim}
  G=lam[s, x, y].
    s1 = F(s, x);
    s2 = F(s1, y);
    o = O(s2);
    return o;
\end{verbatim}}}
\noindent We convert this function into the path formula $A=\bigwedge_{i\in I} \Phi_{i}\rightarrow\overline{o} = \phi_i$. We can similarly calculate a path formula $A'=\bigwedge_{i\in I'} \Phi'_{i}\rightarrow\overline{o'} = \phi'_i$ for the function $O(F(s, yx))$. Verifying the algebraic condition $\forall s,x,y. O(F(s, xy))=O(F(s, yx))$ is then equivalent to proving that $\forall s,x,y. \exists o,o'. A\wedge A'\wedge \overline{o}=\overline{o'}$ is a tautology. The latter problem is equivalent to proving that $\neg \exists o,o'. A\wedge A'\wedge\overline{o}=\overline{o'}$ is unsatisfiable. As mentioned before, we convert this formula into one without quantifiers, which are suitable for SMT solvers, with the following transformation:
\begin{eqnarray}
&&\neg \exists o,o'.A'\wedge A\wedge \overline{o}=\overline{o'} \nonumber\\
%&\equiv& \forall sxy.\exists o,o'.(\bigwedge_{i\in I} \Phi_{i}\rightarrow\overline{o} = \phi_i)\wedge (\bigwedge_{j\in I'} \Phi'_{j}\rightarrow\overline{o'} = \phi'_j)\wedge \overline{o}=\overline{o'}\nonumber\\
%&\equiv& \neg\exists o,o'.(\bigvee_{i\in I} \Phi_{i}\wedge\overline{o} = \phi_i)\wedge (\bigvee_{j\in I'} \Phi'_{j}\wedge\overline{o'} = \phi'_j)\wedge \overline{o}=\overline{o'}\nonumber\\
&\equiv& \neg\exists o,o'.\bigvee_{i\in I, j\in I'} (\Phi_{i}\wedge\overline{o} = \phi_i\wedge\Phi'_{j}\wedge\overline{o'} = \phi'_j\wedge \overline{o}=\overline{o'})\nonumber\\
&\equiv& \neg \bigvee_{i\in I, j\in I'} (\Phi_{i}\wedge\Phi'_{j}\wedge\phi_i=\phi'_j)\nonumber\\
&\equiv& \bigwedge_{i\in I, j\in I'} (\Phi_{i}\wedge\Phi'_{j}\rightarrow\phi_i\neq\phi'_j)\nonumber\\
&\equiv& \bigvee_{i\in I, j\in I'} (\Phi_{i}\wedge\Phi'_{j}\wedge \phi_i\neq\phi'_j)\textrm{~~~(using Lemma~\ref{prop:nb})}\nonumber
\end{eqnarray}
%\noindent The last formula is quantifier free and so can be submitted to a SMT solver for satisfiability checking.

\begin{exmp}
Let us consider the function in Example~\ref{ex:pathf} as an accumulator. Suppose $f$ and $s$ are solution variables, and the output function is the identity function. The query that we submit to the SMT solver is:
\begin{eqnarray}
&&f=1\wedge f=1\wedge (0\neq0\vee 10+x+y\neq 10+y+x)\nonumber\\
&\wedge&f=1\wedge f\neq1\wedge (0\neq f\vee 10+x+y\neq s+y+x)\nonumber\\
&\wedge&f\neq1\wedge f=1\wedge (f\neq0\vee s+x+y\neq 10+y+x)\nonumber\\
&\wedge&f\neq1\wedge f\neq1\wedge (f\neq f\vee s+x+y\neq s+y+x)\nonumber
\end{eqnarray}
This is obviously unsatisfiable and so the reducer is decomposable.
%Notice that the original query should contain four sub-formulae, but we prune two of them that have an unsatisfiable formula $f=1\wedge f\neq 1$ as a component.
\end{exmp}

Although Theorem~\ref{thm:dr} requires that $s$ takes value from $S$, we do not express this restriction here since we cannot calculate $S$ and so we instead require that the equation holds for any $s\in \mathbb{Z}$. This stronger requirement means that our verification is overly conservative and some reduce functions that are decomposable in theory will fail our test. On the other hand, we can also restrict the value of $s$ to take only $s_0$, which is too liberal as some non-decomposable functions can pass verification. In our study, however, we find that restricting $s$ to as $s_0$ works well: all reduce functions that pass this test are decomposable. Restricting $s$ to be $s_0$ brings us another benefit: the verification problem becomes verifying $R(xy)=R(yx)$ and so provides the developers with early feedback about the decomposability of their reduce functions.

\subsection{Combiner Synthesis Algorithms }
\label{sec:com-syn}

As discussed in Section~\ref{sec:theory}, given a decomposable reduce $R=\langle s_0;F;O\rangle$, the combiner $C$ can be constructed as $C(s_1, s_2) = F(s_1, H(s_2))$, where $H$ is any general inverse function of $F$. Unfortunately, constructing $H$ is a nondeterministic program inversion problem whose input cannot be uniquely determined by its output. As far as we know, no existing work considers this problem.

Solving the general nondeterministic program inversion problem is probably too difficult. However, we observe that most accumulators of a decomposable reducer can be classified into three categories according to how they aggregate:
(1) \emph{counting} aggregation over an input sequence that is only determined by the length of the sequence; (2) \emph{single input} aggregation over an input sequence that can always be simulated by an aggregation over one input record; and (3) \emph{finite state machine} aggregation that is essentially simulating a finite state machine.
To leverage this observation, we first develop a criteria to check which category a decomposable reducer belongs to, and then develop an algorithm to synthesize a combiner for each category. Because a reducer can belong to more than one category, we prioritize our classification according to the performance of the synthesized combiners; finite state machine has the highest priority while counting has the lowest with single input in the middle. The point of assigning these priorities will become more clear as we explain the synthesizing algorithm.

\subsubsection*{Counting}
A typical accumulator that belongs to this category is the {\tt COUNT} function built-in most DBMSs. We formally define the criterion of this category as follows:
\begin{definition}[Counting category] An accumulator $F$ belongs to the \emph{counting category} if and only if, $F(s_0, X)=F(s_0, Y)$ holds for any two input sequences $X, Y\in I^*$, $|X|=|Y|$.
\end{definition}
\noindent This criterion can be transformed into an amenable form via the following lemma:
\begin{lemma} An accumulator $F$ belongs to the counting category if and only if, for any two input records $x,y\in I$, $F(s_0, x)=F(s_0, y)$ holds.
\end{lemma}
\begin{proof}[Proof (a sketch)] The necessary direction is obvious. The sufficient direction can be proved by induction on $|X|$ and using the commutativity of $F$.
\end{proof}
\noindent With this lemma, we can use an SMT solver as described earlier to verify that $\forall x,y. F(s_0, x)=F(s_0, y)$. For the combiner synthesis problem, the combiner is exactly the following function:
{\small{\begin{verbatim}
  input(s1, s2);
  s = s0; r = s1;
  while(s <> s2) {
    s = F(s, 0);
    r = F(r, 0);
  }
  return r;
\end{verbatim}}}
\noindent Here we use {\tt s0}, {\tt s1}, and {\tt s2} to represent the initial solution $s_0$, the two input solutions $s_1$ and $s_2$ respectively. We use $s$ and $r$ to represent two local variables. To argue the correctness, suppose that the loop body is executed $n$ times and $X$ is an input sequence of $n$ zeros. We notice that when the loop stops, {\tt r} stores the value of $F(s_1, X)$, and $F(s_0, X)=s_2$. We can then build a general inverse function $H$ of $F$ such that $H(s_2)=X$, and then $r=C(s_1, s_2) = F(s_1, H(s_2))$.

Notice that although we restrict the input functions of our synthesizing algorithm to be in our language, the output combiners can leverage any language features, such as loop, that supported by a MapReduce-style system.
%
Consider the following accumulator function as an exapmle:
{\small{\begin{verbatim}
  F=lam[s, x].return s+2;
\end{verbatim}}}
\noindent The combiner will result in $C(s_1, s_2) = s_1+s_2$.
Although its verification condition is simple, the synthesized combiner will perform an expensive loop at runtime,
and so we assign this category the lowest priority.

\subsubsection*{Single Input}
Most standard accumulators such as {\tt SUM} and {\tt MAX} belong to this category, which is formally defined as follows:
\begin{definition}[Single input category] An accumulator $F$ belongs to the \emph{single input category} if and only if, for any input sequence $X\in I^*$, there is an input record $x\in I$ such that $F(s_0, X)=F(s_0, x)$.
\end{definition}
\noindent The following lemma can be used to produce a more amenable criterion:
\begin{lemma} An accumulator $F$ belongs to single input category if and only if, for any two input records $x,y\in I$, there is an input record $z$ such that $F(s_0, xy)=F(s_0, z)$.
\end{lemma}
\begin{proof}[Proof (a sketch)] The necessary direction is obvious while the sufficient direction can be proven by induction on $|X|$.
\end{proof}
\noindent We then need to verify that $\forall x,y.\exists z. F(s_0, xy) = F(s_0, z)$. However, the techniques that we have discussed so far cannot eliminate the quantifier for $z$ while most existing methods used by SMT solvers to handle quantifiers rely on the E-match algorithm~\cite{e-match}, which cannot deal very well with quantifiers. In fact, our use of Z3~\cite{z3} results in a timeout to check if {\tt MAX} belongs to the single input category.

We must then eliminate the quantifier on $z$. For clarity in the following discussion, we use the notion $x$, $y$, and $z$ instead of $\overline{x}$, $\overline{y}$, and $\overline{z}$. We should keep in mind that all $x$, $y$ and $z$ are vectors of variables.
%
Suppose two path formulas: $F(s_0, z)$ is $A=\bigwedge_{i\in I} \Phi_i(z)\rightarrow\overline{o}=\overline{\phi_i}(z)$ and $F(s_0, xy)$ is $A'=\bigwedge_{i\in I'} \Phi'_i(x,y)\rightarrow\overline{o'}=\overline{\phi'_i}(x,y)$. We first transform our condition into a propositional formula so that it can be verified:
\begin{eqnarray}
&&\forall x,y.\exists z,o,o'. A\wedge A'\wedge \overline{o}=\overline{o'}\nonumber\\
%&\equiv&\forall x,y.\exists z. \bigvee_{i\in I'} \Phi'_i(x,y)\wedge\bigvee_{j\in I} (\Phi_j(z)\wedge \phi'_i(x,y)=\phi_j(z))\nonumber\\
%&\equiv&\forall x,y.\bigvee_{i\in I'} \Phi'_i(x,y)\wedge\bigvee_{j\in I} \exists z. (\Phi_j(z)\wedge \phi'_i(x,y)=\phi_j(z))\nonumber\\
&\equiv&\forall x,y.\bigwedge_{i\in I'} (\Phi'_i(x,y)\rightarrow\bigvee_{j\in I}\exists z.(\Phi_j(z)\wedge \overline{\phi'_i}(x,y)=\overline{\phi_j}(z)))\nonumber
\end{eqnarray}
%
\noindent We then must eliminate the quantifier in:
\begin{equation}
\exists z.(\Phi_j(z)\wedge \overline{\phi'_i}(x,y)=\overline{\phi_j}(z))\label{qtf:z}
\end{equation}
\noindent This problem is difficult in general. However, we observe that in most cases, heuristic rules can work well. Notice that the formula $\Phi_j(z)\wedge \overline{\phi'_i}(x,y)=\overline{\phi_j}(z)$ is a conjunction of clauses. We develop two heuristic rules, each of which makes an equivalent transformation. First, if a variable $v$ in the vector $z$ appears only in a set of clauses which do not contain other variables in $x$, $y$, and $z$, then we issue the conjunction all these clauses as a query to the SMT solvers. If the query is unsatisfiable, then we convert Formula~\ref{qtf:z} into a false; otherwise, we remove all clauses containing $v$, and the existential quantifier of $v$ from Formula~\ref{qtf:z}. Second if one of the clauses in Formula~\ref{qtf:z} can be transform into the form of $v=\alpha$, then we replace any occurrences of $v$ in Formula~\ref{qtf:z} with $\alpha$, and eliminate the clause $v=\alpha$ and the quantifier of $v$ from Formula~\ref{qtf:z}. Here, to transform a clause in form of $\beta=\phi(v)$ into one in form of $v=\alpha$, we relies on the some simple arithmetic transformation rules such as $\beta=C+\phi(v)\Rightarrow\beta-C=\phi(v)$.
\begin{exmp} Consider the following formula:
\[\exists a,b. (a\geq 0) \wedge (a<5) \wedge (10>b+3)\wedge (1+x+y=1+b)\]
\noindent First of all, we find that $a$ appears in $a\geq 0\wedge a<5$ which contains none of $x$,$y$ and $b$. We ask the SMT solver which answers that $a\geq 0\wedge a<5$ is satisfiable. By applying the first heuristic rule, we remove them and get the following formula:
\[\exists b. (10>b+3)\wedge (1+x+y=1+b)\]
\noindent Then we use transformation rules to find a clause $b=x+y$. Applying the second heuristic rule, we get the following formula:
\[10>x+y+3\]
This formula is equivalent to the original one.
\end{exmp}
%; (2) $\beta=\phi(v)+C\equiv\beta-C=\phi(v)$; (3) $\beta=C-\phi(v)\equiv C-\beta=\phi(v)$; (4) $\beta=\phi(v)+C\equiv \beta+C=\phi(v)$; (5) $\beta=C\times\phi(v)\equiv\beta/C=\phi(v)$; (6) $\beta=\phi(v)\times C\equiv\beta/C=\phi(v)$; and (7) $\beta=\phi(v)/C\equiv \beta\times C=\phi(v)$.
%
%there are two easier cases. For the first case, $\overline{\phi_j}$ does not contain $z$ at all, which means $\overline{\phi_j}$ is a vector of constants $\overline{c_j}$. For such a case, formula~\ref{qtf:z} is equivalent to $\overline{\phi'_i}(x, y)=\overline{c_j}$.
%
%For the second case, $z$ can be uniquely determined by the equations $\overline{\phi'_i}(x, y)=\overline{\phi_j}(z)$. More explicitly speaking, suppose the vector of $\overline{\phi_j}$ has $m$ dimensions, and $z$ has $n$ dimensions. Then $m-n$ of all expressions in $\overline{\phi_j}$ are constants, which we denote them as $\overline{\phi_j1}=\overline{c_j}$; the rest $n$ expressions, denoted as $\overline{\phi_j2}$, along with the corresponding dimensions of $\overline{\phi'_i}(x, y)$, denoted as $\overline{\phi'_i2}(x, y)$, forms a linear equation
%
%We observe that for most decomposable accumulators, $\overline{\phi_i}(z)$ is either a list of constants $\overline{\phi_{j}}(z)=\overline{c}$, or composed by a list of constants $\overline{\phi_{j1}}(z)=\overline{c}$ and a list of expressions $\overline{\phi_{j2}}(z)$ that can be inverted.
%In our language, we only consider linear arithmetic and so a list of invertable expressions are essentially calculating a vector $w+\Sigma z$ where $\Sigma^{-1}$ exists.
%
%For the first case (list of constants), since we only consider $\Phi_j(z)$ when it is satisfiable, the quantifier can be eliminated directly in the following way:
%\begin{eqnarray}
%&&\exists z.\Phi_j(z)\wedge \overline{\phi'_i}(x,y)=\overline{\phi_j}(z)\nonumber\\
%&\equiv&\overline{\phi'_i}(x, y)=\overline{c}\nonumber
%\end{eqnarray}
%%
%\noindent For the second case (composed list of constants and expressions), if $\overline{\phi_j^{-1}}$ is the list of inverse expressions of $\overline{\phi_{j2}}(z)$, then we can eliminate the quantifier in the following way:
%\begin{eqnarray}
%&&\exists z.\Phi_j(z)\wedge \overline{\phi'_i}(x,y)=\overline{\phi_j}(z)\nonumber\\
%&\equiv&\exists z.\Phi_j(z)\wedge \overline{\phi'_{i1}}(x,y)=\overline{c}\wedge\overline{\phi'_{i2}}(x,y)=\overline{\phi_j}(z)\nonumber\\
%&\equiv&\exists z.\Phi_j(z)\wedge \overline{\phi'_{i1}}(x,y)=\overline{c}\wedge z=\overline{\phi_i^{-1}}(\overline{\phi'_{i2}}(x,y))\nonumber\\
%&\equiv&\Phi_j(\overline{\phi_i^{-1}}(\overline{\phi'_{i2}}(x,y)))\wedge \overline{\phi'_{i1}}(x,y)=\overline{c}\nonumber
%\end{eqnarray}
%%
%\noindent We therefore transform the original verification problem into an unsatisfiability check of the following formula:
%\begin{eqnarray}
%&\bigvee_{i\in I'}&(\Phi'_i(x,y)\wedge\bigwedge_{j\in I_1} \overline{\phi'_{i1}}(x,y)\neq\overline{c_j}\nonumber\\
%&&\wedge\bigwedge_{j\in J_2}\neg\Phi_j(\overline{\phi_i^{-1}}(\overline{\phi'_{i2}}(x,y)))\vee \overline{\phi'_{i1}}(x,y)\neq\overline{c_j})\nonumber
%\end{eqnarray}
%\noindent Here $I_1$ contains those $i$ in the first case, and $I_2$ contains those $i$ in the second case.

%As an example, consider the MAX function as the accumulator:
%\begin{verbatim}
%  F=lam[s, x].
%    if(s>x) then
%      return s;
%    else
%      return x;
%\end{verbatim}
%Suppose $s_0 = 0$. Then a path formula of $F(s_0, z)$ is
%\[0>z\rightarrow o_1=0 \bigwedge 0\leq z\rightarrow o_1=z\]
%A path formula of $F(s_0, xy)$ is
%\begin{eqnarray}
%&&0>x\wedge 0>y\rightarrow o_1=0\bigwedge0>x\wedge 0\leq y\rightarrow o_1=y\nonumber\\
%&\bigwedge& 0\leq x\wedge x>y\rightarrow o_1=x\bigwedge 0\leq x\wedge x\leq y\rightarrow o_1=y\nonumber
%\end{eqnarray}
%The query is
%\begin{eqnarray}
%&&0>x\wedge 0>y\wedge0\neq 0\wedge 0>0\nonumber\\
%&\vee&0>x\wedge 0\leq y\wedge y\neq 0\wedge 0>y\nonumber\\
%&\vee&0\leq x\wedge x>y\wedge x\neq 0\wedge 0>x\nonumber\\
%&\vee&0\leq x\wedge x\leq y\wedge y\neq 0\wedge 0>y\nonumber
%\end{eqnarray}

Next, we consider how to synthesize a general inverse function $H$. Our solution is inspired by that we can convert a formula back to a program as discussed in Section~\ref{sec:formulae}. Suppose $F$ has a path formula $\bigwedge_{i\in I}\Phi_i(x)\rightarrow o=\phi_i(x)$. We want to find an output formula of $H$ in the form of $(\bigvee_{j\in J}\Phi_j'(s))\wedge\bigwedge_{j\in J}\Phi_j'(s)\rightarrow x=\phi_j'(s)$, such that every interpretation $\langle x,s\rangle$ satisfying the output formula of $H$ also satisfies the path formula of $F$. This means
\[(\bigvee_{j\in J}\Phi_j'(s))\wedge\bigwedge_{j\in J}\Phi_j'(s)\rightarrow x=\phi_j'(s)\models\bigwedge_{i\in I}\Phi_i(x)\rightarrow s=\phi_i(x)\]
We first can make the following equivalent transformation:
\[\bigwedge_{i\in I}\Phi_i(x)\rightarrow o=\phi_i(x)\equiv\bigvee_{i\in I}\Phi_i(x)\wedge o=\phi_i(x)\]
Then we want to make several transformations to find an output formula of $H$. Based on the same observations, we develop two heuristic rules, each of which transforms $\alpha$ into $\beta$ only if $\beta\models\alpha$, and recursively apply them to each variable and each conjunction $\Phi_i(x)\wedge s=\phi_i(x)$. Given a variable $v$ in the vector $x$, first, if $v$ appears only in a set of clauses in the conjunction, which do not contain other variables in $x$ and $s$, then we issue the conjunction of all these clauses as a query, which is satisfiable since it is a component of $\Phi_i(x)$, to the SMT solvers to ask for an answer $v=v_0$.
%This query must be satisfiable by noticing that it is a component of $\Phi_i(x)$ which is satisfiable.
Then we remove all clauses containing $v$, and add $v=v_0$ to the conjunction. Second, if the first rule does not work, $v$ appears more than once, and one of the clauses in the conjunction can be transformed into the form of $v=\alpha$, where $\alpha$ contains only variables in $s$, then we replace any occurrences of $v$ with $\alpha$.
\begin{exmp} Consider the following implication:
\[a\geq 0\wedge a<5\wedge 10>b+3\wedge s=1+b\]
\noindent First of all, we find that $a$ appears in $a\geq 0\wedge a<5$ which contains no $b$ and $s$. We ask the SMT solver which replies an answer $a=1$. By applying the first heuristic rule, we get the following formula:
\[10>b+3\wedge s=1+b\wedge a=1\]
\noindent Then we use transformation rules to find a clause $b=s-1$. Applying the second heuristic rule, we get the following formula:
\[8>s\wedge a=1\wedge b=s-1\]
This formula implies the original formula.
\end{exmp}

Suppose we can successfully get a formula in the form of $\bigvee_{i\in I}\Phi_i'(s)\wedge x=\phi_i'(s)$ using the heuristic rules. It is easy to prove that
\[(\bigvee_{i\in I}\Phi_i'(s))\wedge\bigwedge_{i\in I}\Phi_i'(s)\rightarrow x=\phi_i'(s)\models \bigvee_{i\in I}\Phi_i'(s)\wedge x=\phi_i'(s)\]
while the precondition above can be implied by
\[(\bigvee_{i\in I}\Phi_i'(s))\wedge\bigwedge_{i\in I}((\bigwedge_{j<i}\neg\Phi_j'(s))\wedge\Phi_i'(s)\rightarrow x=\phi_i'(s))\]
This formula is equivalent to an output formula of the following code.
{\small{\begin{verbatim}
    F=lam[x].
    if (cond_1) then
        return output_1;
    else if (cond_2) then
        return output_2;
    ...
\end{verbatim}}}
\noindent where {\tt cond\_}$i$ is the $\Phi_i(s)$, and {\tt output\_}$i$ is $\phi_i$.

\begin{exmp}
Let us again consider the MAX function. Our algorithm will generate the following general inverse function:
{\small{\begin{verbatim}
  H=lam[s].
    if (s>0) then return s;
    else if (s=0) then return s;
\end{verbatim}}}
\noindent The synthesized combiner is
{\small{\begin{verbatim}
  C=lam[s1, s2].F(s1, H(s2))
\end{verbatim}}}
\end{exmp}

\subsubsection*{Finite State Machine}

Every accumulator can be treated as a transition function of a state machine. If this state machine has a small size, then it is easy to synthesize a combiner as we shall show. Given an explicit finite state machine, a general inverse function is equivalent to finding a sequence of inputs that will transform the state machine to a given state.
However, the finite state machine defined by an accumulator is implicit and so (1) we do not know the size of the state machine; and (2) we do not know its transition table.
To tackle these problems, we design a breadth first search algorithm (Algorithm~\ref{alg:sm}) to identify all states by using a set data structure \emph{Sol} to store all explored states as a set of triples ($s$, $ps$, $x$).
Intuitively, $(s, ps, x)$ encodes that $ps$ can be transformed into $s$ by taking an input $x$ and so $s = F(ps, x)$.
If we identify that $F$ is a state machine containing no more than a threshold of $T$ states, the algorithm succeeds along with the code for the combiner; otherwise, if the algorithm has found more than $T$ states, the algorithm will immediately fail.
The key parts of this algorithm are the \emph{find\_new} call (line 4) that finds a new state, and the \emph{generate\_combiner} call at the end that generates the code for the combiner.

\begin{algorithm}[t]
\begin{algorithmic}[1]
\STATE Q.clear(); Q.add($s_0$); \emph{Sol}.add($s_0$, null, null);
\WHILE{not Q.isEmpty()}
    \STATE $ps$ = Q.poll();
    \WHILE{($ns$, $x$) = find\_new($ps$, \emph{Sol})}
        \IF{\emph{Sol}.size() $\geq T$}
            \STATE return (false, null);
        \ENDIF
        \STATE \emph{Sol}.add($ns$, $ps$, $x$);
        \STATE Q.add($ns$);
    \ENDWHILE
\ENDWHILE
\STATE return (true, generate\_combiner(\emph{Sol}));
\end{algorithmic}
\caption{Detecting finite state machine and generating its combiner function.}
\label{alg:sm}
\end{algorithm}

The \emph{find\_new} function finds a new state $ns$ starting from a state $ps$ by taking $x$ as input, and so the following equation holds:
\[ns = F(ps, x) \wedge \bigwedge_{(a, b, c)\in Sol} a\neq ns\]
\noindent We transform this into the following query that can be submitted to the SMT solver:
\[\bigvee_{i\in I} (\Phi_i(ps, x)\wedge ns=\phi_i(ps, x)\wedge \bigwedge_{(a, b, c)\in Sol} a\neq ns)\]
\noindent If the SMT solver returns a solution, then \emph{find\_new} will return $(ns, x)$ as the answer. Otherwise, \emph{find\_new} will return an empty answer.

When all possible states have been explored, they are all stored in \emph{Sol}. To calculate a general inverse function $H$, for each state $s$, we can back-trace from $s$ to $s_0$ according to \emph{Sol} to find a sequence $x_1x_2...x_k$, then we know $H(s_2) = x_k...x_1$. According to the commutativity of $F$, we know $F(s_1, x_1...x_k) = F(s_1, x_k...x_1) = F(s_1, H(s_2)) = C(s_1, s_2)$. The following code implements this idea:
{\small{\begin{verbatim}
// Combiner Setup
1:  Dictionary<State, State> ps = new Dictionary();
2:  Dictionary<State, Input> x = new Dictionary();
3:  ps[*]=*; x[*]=*;
    ...
// Combiner(s1, s2)
    s = s1;
    while(s2 <> s0) {
      s = F(s, x[s2]);
      s2 = ps[s2];
    }
    return s;
\end{verbatim}}}
\noindent The code on line 3 is generated according to \emph{Sol}. For each $(s, ps, x)\in Sol$, we generate the following line of code:
\\
{\small{{\tt ps[$s$]=$ps$; x[$s$]=$x$;}}}
\\
\noindent Furthermore, since there are at most $T$ different states, we can materialize all $T^2$ combinations of $(s_1, s_2)$ and the answers as the combiner $C(s_1, s_2)$, which is also the most efficient combiner.

\begin{exmp}
Let us consider the following code as the accumulator, and $s_0=-1$:
{\small{\begin{verbatim}
  F=lam[s,x].
    if (s == -1) then
      if (x > 100) then return 0; else return 2;
    else if (s == 0) then
      if (x > 100) then return 0; else return 1;
    else if (s == 2) then
      if (x > 100) then return 1; else return 2;
    else return s;
\end{verbatim}}}
\noindent By running our algorithm, a \emph{Sol} is calculated that includes three triples: $(0, -1, 101)$, $(2, -1, 100)$, and $(1, 0, 100)$. The synthesized combiner looks like follows:
{\small{\begin{verbatim}
    input(s1, s2)
    if (s1 == -1 and s2 == -1) then return -1;
    ...
    else if (s1 == 2 and s2 == 0) then return 1;
    ...
\end{verbatim}}}
\end{exmp}
